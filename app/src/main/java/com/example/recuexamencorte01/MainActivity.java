package com.example.recuexamencorte01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtCliente;
    private Button btnCotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cotizacion();
            }
        });
    }

    private void iniciarComponentes() {
        txtCliente = findViewById(R.id.txtCliente);
        btnCotizacion = findViewById(R.id.btnCotizacion);
    }

    private void cotizacion() {
        String textoNombre = txtCliente.getText().toString().trim();

        if(textoNombre.isEmpty()) {
            Toast.makeText(this, "Ingrese el nombre del cliente", Toast.LENGTH_SHORT).show();
        } else {
            // Crear Bundle
            Bundle bundle = new Bundle();
            bundle.putString("nombreCliente", txtCliente.getText().toString());

            // Crear Intent
            Intent intent = new Intent(MainActivity.this, CotizacionActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad
            startActivity(intent);

        }
    }
}